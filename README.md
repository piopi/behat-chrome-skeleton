Behat chrome skeleton
=====================

Skeleton PHP project tested with Behat, Mink and ChromeDriver.

Includes docker image, CI configuration, and specification examples.

## Instructions

* Initiate development environment:

```bash
cd behat-chrome-skeleton
./bin/dev-environment.sh
```

By default it runs a PHP 7.1 container with nginx. You can specify the PHP version with

```
PHP_VERSION=5.6 ./bin/dev-environment.sh
PHP_VERSION=7.0 ./bin/dev-environment.sh
```

* Install dependencies:

```bash
composer install
```

* Run tests:

```bash
vendor/bin/behat
```

* Use ctrl +d when you're done to close and remove the container
